package no.uib.model;

import java.awt.Color;
import java.util.Random;

import no.uib.inf101.sample.eventbus.ColorFieldModelChangedEvent;
import no.uib.inf101.sample.eventbus.EventBus;
import no.uib.inf101.sample.eventbus.EventHandler;
import no.uib.inf101.sample.view.ViewableColorField;

public class ColorFieldModel implements ViewableColorField {

  // Field variables
  private Random random;
  private static final int COLOR_VALUE_BOUND = 256; // NB: STORE BOKSTAVER i variabelnavnet indikerer at dette skal
                                                    // være en *STATIC FINAL* variabel!! Disse må alltid gis en verdi i
                                                    // feltvariabelområdet
  private EventBus bus;
  private int step = 0;
  private static final int NUMBEROFSTEPS = 50;
  private Color oldColor;
  private Color targetColor;

  // Constructor
  public ColorFieldModel() {
    this.bus = new EventBus();
    this.random = new Random();
    this.oldColor = generateRandomColor();
    this.targetColor = generateRandomColor();
  }

  public void disruptWithNewColor() {
    this.oldColor = this.generateRandomColor();
    this.bus.post(new ColorFieldModelChangedEvent(oldColor, this));
  }

  public Color getCurrentColor() {
    return new Color(this.mix(this.oldColor.getRed(), this.targetColor.getRed()),
        this.mix(this.oldColor.getGreen(), this.targetColor.getGreen()),
        this.mix(this.oldColor.getBlue(), this.targetColor.getBlue()));
  }

  private int mix(int oldValue, int targetValue) {
    return ((oldValue * (NUMBEROFSTEPS - this.step)) + (targetValue * this.step)) / NUMBEROFSTEPS;
  }

  private Color generateRandomColor() {
    return new Color(this.random.nextInt(COLOR_VALUE_BOUND), this.random.nextInt(COLOR_VALUE_BOUND),
        this.random.nextInt(COLOR_VALUE_BOUND));
  }

  @Override
  public void registerForEvents(EventHandler handler) {
    this.bus.register(handler);
  }

  public void goStep() {
    if (this.step >= NUMBEROFSTEPS) {
      this.step = 0;
      this.oldColor = this.targetColor;
      this.targetColor = generateRandomColor();
    } else {
      this.step++;
    }
    this.bus.post(new ColorFieldModelChangedEvent(this.getCurrentColor(), this));
  }

  // Ding dong dette er en test

}
