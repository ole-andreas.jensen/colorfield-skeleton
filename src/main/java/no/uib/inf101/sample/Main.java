package no.uib.inf101.sample;

import javax.swing.JFrame;

import no.uib.inf101.sample.controller.ColorfieldController;
import no.uib.inf101.sample.view.ColorFieldView;
import no.uib.model.ColorFieldModel;

/**
 * Start the application. Creates the event bus, model, view and
 * controller, and shows the view in a window.
 */
public class Main {

  /**
   * Start the application.
   *
   * @param args command line arguments (are ignored)
   */
  public static void main(String[] args) {
    ColorFieldModel model = new ColorFieldModel();
    ColorFieldView view = new ColorFieldView(model);
    new ColorfieldController(model, view);

    JFrame frame = new JFrame("Color Field");
    frame.setTitle("Color Field");
    frame.add(view);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);

  }
}
