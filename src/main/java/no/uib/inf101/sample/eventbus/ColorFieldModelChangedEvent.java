package no.uib.inf101.sample.eventbus;

import java.awt.Color;

import no.uib.model.ColorFieldModel;

public record ColorFieldModelChangedEvent(Color color, ColorFieldModel field) implements Event{

}
