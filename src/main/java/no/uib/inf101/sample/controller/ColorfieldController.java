package no.uib.inf101.sample.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;

import javax.swing.Timer;

import no.uib.inf101.sample.view.ColorFieldView;
import no.uib.model.ColorFieldModel;

public class ColorfieldController extends MouseAdapter implements ActionListener {

  // Field variables
  private ColorFieldModel model;
  private ColorFieldView view;

  // Constructor
  public ColorfieldController(ColorFieldModel model, ColorFieldView view) {
    this.model = model;
    this.view = view;
    this.setupTimer();
    view.addMouseListener(this);

  }

  public void mousePressed(MouseEvent mouseEvent) {
    super.mousePressed(mouseEvent);
    Ellipse2D circle = this.view.getCircle(); 
    if (circle.contains(mouseEvent.getPoint())) {
      this.model.disruptWithNewColor();
    }
  }

  @Override
  public void actionPerformed(ActionEvent e) {
    this.model.goStep();
  }

  private void setupTimer() {
    Timer timer = new Timer(100, this);
    timer.start();
  }

}
