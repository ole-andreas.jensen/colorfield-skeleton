package no.uib.inf101.sample.view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JComponent;

import no.uib.inf101.sample.eventbus.Event;
import no.uib.inf101.sample.eventbus.EventHandler;
import java.awt.geom.Ellipse2D;

public class ColorFieldView extends JComponent implements EventHandler {

  // Field variables
  private ViewableColorField field;

  // Constructor
  public ColorFieldView(ViewableColorField field) {
    this.field = field;
    this.field.registerForEvents(this);
  }

  public Ellipse2D getCircle() {
    return new Ellipse2D.Double(0, 0, this.getWidth(), this.getHeight());
  }

  @Override
  public void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D) g;

    Ellipse2D circle = this.getCircle();
    Color color = field.getCurrentColor();
    g2.setColor(color);
    g2.fill(circle);
  }

  @Override
  public void handle(Event event) {
    this.repaint();
  }

}
