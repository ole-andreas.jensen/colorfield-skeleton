package no.uib.inf101.sample.view;

import java.awt.Color;

import no.uib.inf101.sample.eventbus.EventHandler;

public interface ViewableColorField {

  public void registerForEvents(EventHandler handler);

  public Color getCurrentColor();

}
